Python Sudoku HowTo


Create a sudoku:
pysdk.py -c --difficulty=normal --handicap=0 --region_width=3 --region_height=3 sudoku.sdk
This will create a sudoku with normal difficulty and handicap 0 of 3x3 cells
of 3x3 regions and save it to sudoku.sdk. These are the default values.
Difficulty can be easy, normal or hard.
Handicap are the extra numbers added to the sudoku after its creation.

Solve a sudoku:
pysdk.py sudoku.sdk [solved.sdk]
This will solve sudoku.sdk and save it (optionally) to solved.sdk

Run the GUI:
pysdk-gui.py --difficulty=normal --handicap=0 --region_width=3 --region_height=3 [sudoku.sdk]
This will open the graphical mode and create a sudoku with normal difficulty
 and handicap 0 of 3x3 cells of 3x3 regions or optionally open sudoku.sdk.

Save as a image:
pysdk-image.py --width 300 --height 300 --background white --lines black --font FreeSans.ttf --font_size 24 --font_colour black sudoku.sdk sudoku.png
This will create a PNG image (format is obtained from filename extension)
named sudoku.png with size 300x300, white background, black lines and numbers,
numbers with font FreeSans.ttf and size 24. These are the default values.
Formats can be the formats supported by PIL (png, jpeg, bmp, pdf, etc).<br />
Each colour must be a CSS3-style colour string (white, black, grey, darkgrey,
etc).

pysdk-image.py sudoku.sdk sudoku.png
This will create a image named sudoku.png using the default values.<br />

pysdk-image.py --format png sudoku.sdk sudoku.FOO
This will create a PNG image without check the filename extension.

Print sudokus
pysdk-pdf.py -p --print_command="lpr" --page A4 --title_font Helvetica --title_colour black --title_size 72 --filename_font Helvetica --filename_colour black --filename_size 24  --lines black --font Helvetica --font_size 40 --font_colour black sudoku.sdk
This will print with the command lpr one sudoku in A4 size, title with font
Helvetica, colour black and size 72, filename with font Helvetica, colour
black and size 40 and numbers with font Helvetica, color black and size 24.
These are the default values in posix systems, in windows the print command
by default is "AcroRd32.exe /p".
Execute pysdk-pdf.py --show_fonts to get the available fonts.

pysdk-pdf.py -p --four sudoku1.sdk sudoku2.sdk sudoku3.sdk sudoku4.sdk
This will print four sudokus in a page.

Save as PDF:
pysdk-pdf.py sudoku.sdk sudoku.pdf
This will create a PDF (same as the printed version) named sudoku.pdf using
the default values.

pysdk-pdf.py --four sudoku1.sdk sudoku2.sdk sudoku3.sdk sudoku4.sdk sudoku.pdf
This will create a PDF (same as the printed version) named sudoku.pdf using
the default values.

You can use the same options than in Print.

Change default values:
To change the default values edit the config.py file.
